// Bring Mongoose into the app
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Build the connection string
var dbURI = 'mongodb://localhost:27017/authorization';

before(function(done){
  // Create the database connection
  mongoose.connect(dbURI, { useMongoClient: true });

  // When successfully connected
  mongoose.connection.once('open', function(){
    console.log('##### Connection has been made! #####');
    console.log();
    done();
  }).on('error', function (err){
    console.log('Mongoose default connection error: ' + err);
  });
});

