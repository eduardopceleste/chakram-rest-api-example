const chakram = require('chakram'), expect = chakram.expect;
const mongoose = require('mongoose');

data = require('../data.json');
testConfig = require('../config.json');

describe("Migration API", function() {

    var tokenAdmin;

    before("Login Admin and Delete the User Collection - MongoDB", function() {
        var UserSchema = require('../schemas/userSchema');

        UserSchema.remove({ 'merchants.merchantId': 500083}, function(err){
            if(err) return handleError(err);
        });

        //chakram.startDebug();
        return chakram.post(testConfig.APP_URL + "login", {
                "login": "admin@pagosonline.com",
                "password": "Abcd@12345"
        }, {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            json: true,
        })
        .then(function(response) {
            expect(response).to.have.status(200);
            expect(response.body.login).to.equal('admin@pagosonline.com');
            tokenAdmin = response.body.merchants[0].token;

        });


    });

    it('1. Login with invalid credentials - Admin', function() {
        var request = chakram.post(testConfig.APP_URL + 'login', {
            body: {
                "login": "admin@pagosonline.com",
                "password": "Abcd@123456"
            },
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            }
        });

        return expect(request).to.have.status(400);
    });

    it('2. Get all Merchants', function() {
        var request = chakram.request("GET", testConfig.APP_URL + 'migration/merchants', {
            headers: {
                'Authorization': 'Bearer ' + tokenAdmin,
                'Content-Type': 'application/json;charset=UTF-8'
            }
        });

        return expect(request).to.have.status(200);
    });

    it('3. Get specific merchant', function() {
        return request = chakram.get(testConfig.APP_URL + 'migration/merchants/' + data.ID_MERCHANT, {
            headers: {
                'Authorization': 'Bearer ' + tokenAdmin,
                'Content-Type': 'application/json;charset=UTF-8'
            }
        })
        .then(function(response) {
            expect(response).to.have.status(200);                    
            expect(response.body.id).to.equal(500084);
        });
    });

    it('4. Migration Merchant - Blocked User', function() {
        return request = chakram.post(testConfig.APP_URL + 'migration/merchants/', {
                "loginRootUser": data.LOGIN_MERCHANT_BLOCKED,
                "merchantId": data.ID_MERCHANT_TO_MIGRATE
            }, {
            headers: {
                'Authorization': 'Bearer ' + tokenAdmin,
                'Content-Type': 'application/json;charset=UTF-8'
            }
        })
        .then(function(response) {
            expect(response).to.have.status(400);                    
        });
    });

    it('5. Migration Merchant - User from other Merchant', function() {
        return request = chakram.post(testConfig.APP_URL + 'migration/merchants/', {
                "loginRootUser": data.LOGIN_MERCHANT,
                "merchantId": data.ID_MERCHANT_TO_MIGRATE
            }, {
            headers: {
                'Authorization': 'Bearer ' + tokenAdmin,
                'Content-Type': 'application/json;charset=UTF-8'
            }
        })
        .then(function(response) {
            expect(response).to.have.status(400);                    
        });
    });

    it('6. Migration Merchant - Login isnt an email', function() {
        return request = chakram.post(testConfig.APP_URL + 'migration/merchants/', {
                "loginRootUser": data.LOGIN_MERCHANT_TO_MIGRATE_INVALID,
                "merchantId": data.ID_MERCHANT_TO_MIGRATE
            }, {
            headers: {
                'Authorization': 'Bearer ' + tokenAdmin,
                'Content-Type': 'application/json;charset=UTF-8'
            }
        })
        .then(function(response) {
            expect(response).to.have.status(400);                    
        });
    });

    it('7. Migration Merchant - All Users from one specific merchant', function() {
        return request = chakram.post(testConfig.APP_URL + 'migration/merchants/', {
                "loginRootUser": data.LOGIN_MERCHANT_TO_MIGRATE,
                "merchantId": data.ID_MERCHANT_TO_MIGRATE,
                "migrateUsers": true
            }, {
            headers: {
                'Authorization': 'Bearer ' + tokenAdmin,
                'Content-Type': 'application/json;charset=UTF-8'
            }
        })
        .then(function(response) {
            expect(response).to.have.status(200);                    
        });
    });

    after('Delete the Merchant Collection - MongoDB', function() {
        var MerchantSchema = require('../schemas/merchantSchema');        

        MerchantSchema.remove({ _id: "500083"}, function(err){
            if(err) return handleError(err);
        });
    });
});
