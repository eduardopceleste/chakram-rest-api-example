const mongoose = require('mongoose');
const Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

const MerchantSchema = new Schema({
    _id: Number
});

const Merchants = module.exports = mongoose.model('merchants', MerchantSchema);